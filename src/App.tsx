import React from "react";

import { Main } from "./components/main";
import { About } from "./components/about";
import { Footer } from "./components/footer";
import { Social } from "./components/social";
import { Contact } from "./components/contact";
import { Projects } from "./components/projects";
import { Experience } from "./components/experience";

import data from "./data.json";
import { Header } from "./components/header";

function App() {

  const AppRouter = [
    { name: "About", onClick: "#about" },
    { name: "Experience", onClick: "#experience" },
    { name: "Projects", onClick: "#projects" },
    { name: "Contact", onClick: "#contact" },
  ];

  return (
    <div className="App">
      <div className="container">
        <div className="row">
          <div className="col-md-1">
            <Header links={AppRouter} />
            <Social className="social-main" />
          </div>
          <div className="col-md-11">
            <Main email={data.email} />
            <About profile_image={data.profile_image} />
            <Experience />
            <Projects />
            <Contact email={data.email} />
            <Footer />
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
