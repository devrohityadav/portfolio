import { Card } from "./common";
import data from "../data.json";

export const Projects = () => {
  return (
    <div
      id="projects"
      className="container"
      style={{ margin: "0px auto 200px" }}
    >
      <div className="row ">
        <h1 className="section-title mb-5">Some Things I’ve Built</h1>
        {data.projects.map(({ link, image, title, description }, index) => (
          <Card key={index} link={link} image={image} title={title} description={description} />
        ))}
      </div>
    </div>
  );
};