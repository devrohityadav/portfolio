import React from "react";

import { Social } from "./social";

export const Footer = () => {
  return (
    <footer className="container text-center">
      <div className="row">
        <Social className="social-secondary mb-4" />
        <p style={{fontSize: '12px'}}>Created with ❤ by Rohit Yadav</p>
      </div>
    </footer>
  );
};
