import { useTrail, animated } from "react-spring";

import data from "../data.json";

interface Props {
  className: string;
}

export const Social = ({ className }: Props) => {
  const trail = useTrail(data.social_meida_accounts.length, {
    config: { mass: 1, tension: 100, friction: 10 },
    from: { left: -50, opacity: 0 },
    to: { left: 0, opacity: 1 },
  });
  
  return (
    <div className={className}>
      {trail.map((style, index) => (
        <animated.a
          key={index}
          target="_blank"
          rel="noreferrer"
          className="social-media-icon p-2"
          style={{position: 'relative',...style}}
          title={data.social_meida_accounts[index].platform}
          href={data.social_meida_accounts[index].profile}
        >
          <i className={data.social_meida_accounts[index].icon}></i>
        </animated.a>
      ))}
    </div>
  );
};
