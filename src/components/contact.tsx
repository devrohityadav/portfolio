import React from "react";

import { Button } from "./common";

interface Props {
  email: string;
}

export const Contact = ({ email }: Props) => {
  return (
    <div
      id="contact"
      className="container text-center"
      style={{ maxWidth: "600px", margin: "0px auto 200px" }}
    >
      <h1 className="section-title">Get In Touch</h1>
      <p style={{ marginBottom: 24, padding: 24 }}>
        Whether you have an idea for a project or just want to chat, feel free
        to shoot me an email!
      </p>
      <Button href={`mailto:${email}`} text="Say Hello" />
    </div>
  );
};
