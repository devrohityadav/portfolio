import { useState } from "react";
import { animated, interpolate, useSpring } from "react-spring";

interface HeaderLink {
  name: string;
  onClick: string;
}

interface Props {
  links: HeaderLink[];
}

export const Header = (props: Props) => {
  const [open, setOpen] = useState(false);
  const { translate, ...rest } = useSpring({
    translate: open ? "-50%" : "-100%",
    opacity: open ? 1 : 0,
  });

  const spring = useSpring({
    config: { mass: 1, tension: 100, friction: 10 },
    from: { left: -50 },
    to: { left: 0, top: 0 },
  });
  return (
    <animated.div style={{ position: "fixed", ...spring }}>
      {open && (
        <div
          onClick={() => setOpen(!open)}
          style={{
            position: "fixed",
            top: 0,
            bottom: 0,
            right: 0,
            left: 0,
            background: "rgba(0,0,0,0.5)",
          }}
        ></div>
      )}
      <i
        className={`fas ${open ? "fa-times" : "fa-bars"}`}
        style={{
          position: "absolute",
          zIndex: 2,
          left: "32px",
          top: "16px",
          background: "#000",
          color: "#fff",
          borderRadius: "50%",
          padding: 16,
          boxShadow: "0 5px 8px 0 rgba(0,0,0,0.9)",
        }}
        onClick={() => setOpen(!open)}
      ></i>

      <animated.div
        style={{
          position: "absolute",
          top: 0,
          left: 0,
          background: "rgba(0,0,0, 0.9)",
          borderRadius: "400px",
          width: "800px",
          height: "800px",
          zIndex: 1,
          ...rest,
          // @ts-ignore
          transform: interpolate(
            // @ts-ignore
            [translate],
            (trans) => `translate(${trans}, ${trans})`
          ),
        }}
      >
        <animated.ul
          style={{
            position: "absolute",
            top: "55%",
            left: "60%",
            padding: 16,
            listStyle: "none",
            ...rest,
          }}
        >
          {props.links.map((link, index) => (
            <li key={index}>
              <a className="menu-link" href={link.onClick}>
                {link.name}
              </a>
            </li>
          ))}
          <li>
            <a href="https://rohityadav.s3.ap-south-1.amazonaws.com/resume.pdf" target="_blank" rel="noreferrer" className="menu-link">
              Resume
            </a>
          </li>
        </animated.ul>
      </animated.div>
    </animated.div>
  );
};
