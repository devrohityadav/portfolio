import React from "react";
import { Button } from "./common";

interface Props {
  email: string;
}

export const Main = ({ email }: Props) => {
  return (
    <div className="container" style={{ margin: "100px auto 200px" }}>
      <div className="row">
        <p className="title">Hi, my name is</p>
        <h1 className="section-title" style={{ fontSize: "4rem" }}>
          Rohit Yadav.
        </h1>
        <h1 className="section-title text-muted mb-5" style={{ fontSize: "4rem" }}>
          I build things for the web.
        </h1>
        <div className="mb-5" style={{maxWidth: "500px"}}>
          <p>
            I'm a software developer based in Shillong, India specializing in
            building and designing exceptional websites,
            applications, and everything in between.
          </p>
        </div>
        <div>
          <Button href={`mailto:${email}`} text="Get In Touch" />
        </div>
      </div>
    </div>
  );
};
