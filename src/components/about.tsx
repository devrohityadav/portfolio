interface Props {
  profile_image: string;
}

export const About = ({ profile_image }: Props) => {
  return (
    <div id="about" className="container" style={{margin: '0 auto 200px'}}>
      <div className="row">
          <h1 className="section-title mb-5 text-left">About Me</h1>
          <div className="col-md-6">
            <p className="mb-4">
              Hello! I'm Rohit, a software developer based in Shillong, India.
            </p>
            <p className="mb-4">
              I enjoy creating things that live on the internet, whether that be
              websites, applications, or anything in between. My goal is to
              always build products that provide awesome experiences.
            </p>
            <p className="mb-4">
              I recently graduated from
              <a href="https://sec.edu.in/" target="_blank" rel="noreferrer">
                {" "}
                St. Edmund's College Shillong{" "}
              </a>
              with a degree in Bachelor of Computer Applications.
            </p>
            <p className="mb-4">
              When I'm not in front of a computer screen, I'm probably reading
              books, enjoying with friends, or just roaming around.
            </p>
          </div>
        <div className="col-md-6 text-center">
          <img
            width="300"
            height="300"
            alt="profile_image"
            src={profile_image}
          />
        </div>
      </div>
    </div>
  );
};
