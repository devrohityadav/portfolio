import React from "react";

interface CardProps {
  link: string;
  title: string;
  image: string;
  description: string;
}

export const Card = ({ link, image, title, description }: CardProps) => (
  <a
    href={link}
    target="_blank"
    rel="noreferrer"
    className="card text-center p-4 m-2"
    style={{ width: 300, border: "none", background: "transparent" }}
  >
    <img className="card-img-top" width={200} height={200} src={image} alt={title} />
    <div className="card-body text-white">
      <p className="card-title">{title}</p>
      <h6 className="card-text">{description}</h6>
    </div>
  </a>
);
