interface Props {
  href: string;
  text: string;
}

export const Button = ({ href, text }: Props) => (
  <a href={href} className="get-in-touch">
    {text}
  </a>
);
