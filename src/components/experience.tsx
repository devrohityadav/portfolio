import React, { useState } from "react";

import data from '../data.json';

export const Experience = () => {
  return (
    <div id="experience" className="container" style={{ margin: "0px auto 200px" }}>
      <div className="row">
        <h1 className="section-title mb-5">Where I’ve Worked</h1>
        <ExperienceTabs />
      </div>
    </div>
  );
};

const ExperienceTabs = () => {
  const [active, setActive] = useState(0);

  return (
    <div className="experiences">
      <div
        role="tablist"
        aria-orientation="vertical"
        className="experience nav nav-pills mt-4 me-4"
      >
        {data.experiences.map((experience, index) => (
          <span
            role="tab"
            key={index}
            data-bs-toggle="tab"
            id={`v-pills-${index}-tab`}
            onClick={() => setActive(index)}
            aria-controls={`v-pills-${index}`}
            className={`tab nav-link ${active === index ? "active-tab" : ""}`}
          >
            {experience.company}
          </span>
        ))}
      </div>
      <div className="tab-content" style={{flex: 3}}>
        {data.experiences.map((experience, index) => (
          <div
            key={index}
            role="tabpanel"
            id={`v-pills-${index}`}
            aria-labelledby={`v-pills-${index}-tab`}
            className={`tab-pane fade ${active === index ? "show active" : ""}`}
          >
            <span className="section-title">{experience.position} </span>
            <a className="hvr-sweep-to-right" href={experience.link} target="_blank" rel="noreferrer">
              @ {experience.company}
            </a>
            <p style={{ fontSize: "0.9rem" }}>{experience.duration}</p>
            <ul>
              {experience.details.map((detail, index) => (
                <li key={index}>{detail}</li>
              ))}
            </ul>
          </div>
        ))}
      </div>
    </div>
  );
};
